# Conditionals
# Conditional statements are used to execute some section of code if certain conditions are met

num1 = 2
num2 = 4
num3 = 4

# Conditional statements result into a boolean value: True or False
# If condition is met, execute this code

# Examples of conditions
# == equal to
# > greater than
# >= greater than or equal to
# < less than
# <= less than or equal to
if num1 < num2:
    # If this statement is True, the following will be printed
    print("num1 is less than num2")

if num2 < num1:
    # If this statement is True, the following will be printed
    print("you won't see this in your output")

if num2 == num3:
    print("These variables have the same value, therefore this is being shown")

# You can also do a negated equals with !=
if num1 != num3:
    print("num1 (2) does not equal num3 (4), so this statement is true")

# You can also negate any expression
if not(num2 == num3):
    print("num2 is equal to num3, but we want this code to execute if the expression is NOT True")


# For more complicated logic, you can nest conditional statements
if num2 == num3:
    if num1 < num2:
        print("we made it through 2 conditional statements!")

# The 2 conditions above can be done on the same line
# if condition1 and condition2 are BOTH true, execute this code
if num2 == num3 and num1 < num2:
    print("both of these statements are true")

if num1 == num3 and num2 == num3:
    print("the first condition is False, therefore the entire condition is false")

# in the above example, programming languages do whats called a "short-circuit"
# Since the first condition is false, and the statement is reliant on both statements being true,
# the second condition is never actually evaluated (this is important when working with the advanced datatypes)

# If you have code that you want executed in a few different situations, you can 'or' the statements
# This evaluates the entire condition as True if at least one of the conditions is True. This also
# "short-circuits", so as soon as a True condition is found, it begins executing the code block
if True or False:
    print("One of the conditions was True, so we're here")

if False or True:
    print("One of the conditions was True, so we're here")

if False or False:
    print("We won't see this because there are no True conditions")

if True or True:
    print("This is printed at the first True")

# Conditional logic is based on Boolean Algebra in Discrete Mathematics. Some expressions can be written
# in multiple ways. For example:
#     if not condition1 and not condition2
# can be rewritten as
#     if not (condition1 or condition2) # De Morgan's law

# So let's say you want to execute code A if a condition is met, and if it is not, then you want to execute
# code B instead. You use an 'else' statement. An else statement is executed if the corresponding if statement
# is False
if 4 < 2:
    print("How on earth is 4 less than 2?")
else:
    print("All is right with the world")

# The world isn't quite that black and white. You can also nest this type of structure
if 1 < 2:
    if 5 < 2:
        print("Here we go again...")
    else:
        print("1 is less than 2, but 5 is not, so here we are")
else:
    print("this else statement is attached to the outer if statement, not the inner")

if 2 < 1:
    print("No")
else:
    if 2 == 2:
        print("2 is equal to 2")
    else:
        print("whelp")

# Nested statement structures can be hard to follow because of all of the indentation. The above can
# actually be re-written to be more readable using an 'elif' statement. An elif statement is chained with
# an if statement. If the first condition isn't met, you can still specify another (and more) condition to check
# before going to the catch-all else statement
grade = 72
if grade >= 90:
    print("You got an A")
elif grade >= 80:
    print("You got a B")
# finish this chain for the remaining grades
___



# I'm going to give you the code for getting a random integer between 1 and 5 (inclusive)
import random
randomNum = random.randint(1,5)

# We're going to make a simple guessing game:
# Your turn to prompt the user for input of a number between 1 and 5 (inclusive)
# If they don't provide a valid number, you should print a statement telling them they need
# to follow directions. Otherwise, if they do provide a valid guess, you should tell them if
# their guess was correct, less than the target number, or higher than the target number
____

