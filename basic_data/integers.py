# Basic Datatypes: Integers
# Integers are whole numbers, both positive and negative
# Examples: -423497, -2, 0, 1, 234, 5938495

# Integers can be printed to output
print(-423497)
print(-2)
print(0)
print(1)
print(234)
print(5938495)

# Try printing an Integer
print(____)
