# Basic Datatypes: Strings
# Strings are zero or more characters put together
# Strings are represented by two double-quotes ""
# or two single-quotes ''

# Strings can be a word
print("Hello")

# Strings can be a sentence
print("Programming in Python is easy peasy!")

# Strings can be a single character
print("A")

# Strings can be empty
print("")

# A string using single-quotes
print('this string is using single-quotes! :O')

# Try printing a string
print(____)
