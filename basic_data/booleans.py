# Basic Datatypes: Boolean
# Booleans are a binary value represented by True or False

# Booleans can be printed as well
print(True)

# Try printing False
print(____)

# Different datatypes have boolean representations
# You can check the boolean value of something by using the bool() function
# Integers: 0 is False, everything else is True
print(bool(0)) # False
print(bool(-1)) # True
print(bool(5)) # True

# Floats: 0.0 is False, everything else is True
print(bool(0.0)) # False
print(bool(-0.1)) # True
print(bool(5.2)) # True

# Strings: An empty string "" or '' is False, everything else is True
print(bool("")) # False
print(bool('')) # False
print(bool("False")) # True
print(bool("Three word string")) # True

