# Basic Datatypes: Floats
# Floats are fractional numbers
# Examples: -14.2, 0.0, 0.9458394, 123.456789

# Integers can be printed to output
print(-14.2)
print(0.0)
print(0.9458394)
print(123.456789)

# Try printing a float value
print(____)
