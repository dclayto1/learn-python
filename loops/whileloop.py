# Loops: While Loop
# A while loop is similar to a for-loop in that it executes a block of code a number of times.
# However, unlike a for-loop, a while loop runs as long as its condition is True

# Example on using a while loop in a similar fashion to a for-loop
iterator = 0
while iterator < 5:
    iterator += 1
    print(iterator)

# this is the same as
# for iterator in range(5):
#     print(iterator)

# The code block of a while-loop will only execute if the condition is True
while False:
    print("this is not going to get printed out")

# It is important to have a way for the condition to terminate the loop, otherwise it will never end
# Uncomment to see what happens if you have a loop that never ends.
# Spoiler: it'll just keep printing forever
#while True:
#    print("lol")

# Any loop can actually be ended by using the 'break' statement
i = 0
while True:
    i += 1
    print(i)
    if i == 4:
        print('stopping the loop')
        break

# break statements should be used carefully/sparingly as they _can_ make code harder to read

# While loops are also heavily used in video games. Typically the entire game runs on a While loop called
# the "Game Loop." The 'game' below will continue until the user types in 'q'
playing = True
while playing:
    print("s - shoot")
    print("d - drink")
    print("q - quit")
    choice = input("Enter the corresponding letter of what you would like to do: ")

    if choice == 's':
        print("You shot the guy")
    elif choice == 'd':
        print("You drank some beer")
    elif choice == 'q':
        print("Thanks for playing!")
        playing = False
    else:
        print("Invalid choice. Try again.")

# You can let users type in any number of entries as long as you give them the opportunity to exit the loop
numberOfEntries = 0
total = 0
hasQuit = False
while not hasQuit:
    entry = input("Enter a number between 1 and 10, or press q to quit: ")
    if entry == 'q':
        hasQuit = True
    elif entry.isdigit() and int(entry) >= 1 and int(entry) <= 10:
        # isdigit() checks if the string contains ONLY digits. negatives and floats return False
        # since it was True and we have an 'and' conditional, we can safely cast to an Integer
        # knowing that that it is a valid digit for our purposes
        numberOfEntries += 1
        total += int(entry)
    else:
        print("Invalid entry. Try again.")

if numberOfEntries > 0:
    avg = total / numberOfEntries
    print("The average is: " + str(avg))
else:
    print("You didn't enter any valid entries so we could not calculate an average.")

# We can do a simple rock-paper-scissors now!
import random
playing = True
while playing:
    randChoice = random.randint(1,3)
    randChoiceText = ''
    if randChoice == 1:
        randChoiceText = "Rock"
    elif randChoice == 2:
        randChoiceText = "Paper"
    else:
        randChoiceText = "Scissors"


    print("1 - Rock\n2 - Paper\n3 - Scissors\nq - Quit")
    userChoice = input("Enter your choice: ")
    if userChoice == 'q':
        print("Thank you for playing!")
        playing = False
    elif userChoice.isdigit() and int(userChoice) >= 1 and int(userChoice) <= 3:
        userChoice = int(userChoice)
        if userChoice == 1:
            print("You chose: Rock")
        elif userChoice == 2:
            print("You chose: Paper")
        else:
            print("You chose: Scissors")

        print("The computer chose: " + randChoiceText)

        if userChoice == randChoice:
            print("Tie Game!")
        elif ((userChoice == 1 and randChoice == 2)
             or (userChoice == 2 and randChoice == 3)
             or (userChoice == 3 and randChoice == 1)): # Rock crushes scissors
            print("Computer Wins!")
        else:
            print("You Win!")
    else:
        print("Invalid choice. Try again.")





# Let's expand on our guessing game from the conditionals tutorial!
# This time we're going to have the user continue guessing until they get the answer right
# Have the user guess a number between 1 and 100 (inclusive)
# Keep track of how many guesses it takes them to get the right answer
# Let the user know if their guess was correct and how many guesses they made, or if they guessed
# too low, or too high
randomNum = random.randint(1,100)

____
