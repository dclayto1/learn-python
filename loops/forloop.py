# Loops: For-Loop
# Typically in programming, you need to do a certain set of operations several times in a row.
# Instead of re-writing the set of operations (algorithm) X number of times, we can use a for-loop
# to run the same set of operations as many times as we specify

# In python there is a very handy function that is often used in for-loops
# It is called the range() function. It takes in 1-3 parameters. 
# 1) range(num) -> where num is the stopping point of the returned range that starts at 0
#     example: range(5) == (0,1,2,3,4)
# 2) range(start, stop) -> where start is the number to start the sequence at, and stop is where it stops
#     example: range(2,10) == (2,3,4,5,6,7,8,9)
# 3) range(start, stop, step) -> same as above, but step is the amount between each number in the sequence
#     example: range(2, 10, 2) == (2,4,6,8)
#
# We can use this function to specify the number of times to execute a block of code
for i in range(10):
    print(i)
# The variable i in this for loop has the value of the current step it is at in the range sequence

# j is created inside the for-loop, its life ends when the for loop has finished its course
# j is always 2 in this case since at the start of the loop, j is set to 0, and then 2 is added to it
for i in range(5):
    j = 0
    j = j + 2
    print(j)

# countString is created outside the for-loop, so its reference is accessible outside as well as inside
# therefore we can alter the value of countString on each iteration and have a persistent outcome
countString = ""
for i in range(3, 22, 3):
    countString = countString + str(i) + ","
print(countString)


# we can also loop over the characters of a string datatype
initialWord = "Hello World!"
vowellessWord = ""
for character in initialWord:
    if not (character == "a" or character == "e" 
    or character == "i" or character == "o"
    or character == "u" or character == "y"):
        vowellessWord  = vowellessWord + character
print(vowellessWord)
        
a = 0
b = 0
c = 0
word = "alkasdfbjkabcuwierascbaskdfhaw;ielrhasdcbasdcbaskdfbakwlefbacbasdcbasdcbkjsdbfkajsdbckjbasdckjbascbsdf"
for character in word:
    if character == "a":
        a += 1
    elif character == "b":
        b += 1
    elif character == "c":
        c += 1
print("'a' occurred " + str(a) + " times.")
print("'b' occurred " + str(b) + " times.")
print("'c' occurred " + str(c) + " times.")

# loops also come in handy when reading files
# normally we don't know the exact number of lines in a file, so instead of trying to figure that out
# we can just iterate over every line
evenNumbers = 0
oddNumbers = 0
numberOfThrees = 0
with open('example.txt', 'r') as exampleFile:
    for line in exampleFile:
        #eachLine is a string datatype
        if int(line) % 2 == 0:
            evenNumbers += 1
        else:
            oddNumbers += 1

        # we can also do nested loops
        for character in line:
            if character == "3":
                numberOfThrees += 1

print("Even Numbers: " + str(evenNumbers))
print("Odd Numbers: " + str(oddNumbers))
print("Number of 3's that occurred: " + str(numberOfThrees))


# Lets have a user enter in 5 numbers and add them together
# Example input from user:
# 1
# 5
# 5
# 2
# 2
# Expected output: 1+5+5+2+2 = 15
____
