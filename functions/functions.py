# Functions
# Functions are a way to write reusable code. A function can take in parameters (values) as
# input fields to change the outcome of a function. A function can "return" values or modify data.

# A function is defined with the 'def' keyword, followed by the name of the function, and then 0 or
# more parameter variables.
def firstFunction():
    print("Inside my first function!")

# Note the indentation. Remember that Python is a language of indentation. Indentation defines the
# scope of a block of code.
print("This is the first thing to be printed")

# Note that the above print statement is printed first, before the one in the function. The reason
# for this is that the "def firstFunction():" is a function definition. It's saying that when this
# function is called/invoked, do the code blocked defined below. What does this mean? We need to
# call the function!
firstFunction() # "Inside my first function!" should print out at this point

# Our first function isn't very useful, so lets try making something a little more "dynamic."
# By dynamic, I mean different output depending on the values we give it as input parameters
def functionWithParameter(firstName):
    print("Hello, " + firstName)

functionWithParameter("Dylan")
functionWithParameter("Jordan")
functionWithParameter("Micah")

# We can even do math! This example will also show how to "return" a value
def f(x):
    return x+8

y = f(4) # y is equal to the return value of the function when the input variable is 4 -> x+8 where x == 4
print(y)
y = f(2) # y is equal to the return value of the function when the input variable is 2 -> x+8 where x == 2
print(y)


# Force = mx+b
def calculateForce(m, x, b):
    force = m*x + b
    print("I'm printing inside this function, but also returning a value!")
    return force

# A "returned" value doesn't have to be stored in a variable. It can just be used in place like so:
print(calculateForce(2, 3, 10))
# This prints the value that is returned from the function calculateForce
# Which is the same as doing this
tmpForce = calculateForce(2,3,10)
print(tmpForce)

# Functions can call other functions from within
def funcA(a):
    print("I'm in funcA")
    return funcB(a)

def funcB(hello):
    print("I'm in funcB")
    return "This following value was passed into funcA, passed to funcB, used in this string which is returned to funcA, which returns to the original caller: " + hello

print(funcA("Wild Ride"))


# Functions can be used to help clean up code and make it more readable/maintainable
# Take this example from the 'while loop' section
'''
playing = True
while playing:
    print("s - shoot")
    print("d - drink")
    print("q - quit")
    choice = input("Enter the corresponding letter of what you would like to do: ")

    if choice == 's':
        print("You shot the guy")
    elif choice == 'd':
        print("You drank some beer")
    elif choice == 'q':
        print("Thanks for playing!")
        playing = False
    else:
        print("Invalid choice. Try again.")
'''

# Now with functions
def printMenu():
    print("s - shoot")
    print("d - drink")
    print("q - quit")

def action(choice):
    if choice == 's':
        print("You shot the guy")
    elif choice == 'd':
        print("You drank some beer")
    elif choice == 'q':
        print("Thanks for playing!")
        return False
    else:
        print("Invalid choice. Try again.")

    return True


playing = True
while playing:
    printMenu()
    choice = input("Enter the corresponding letter of what you would like to do: ")
    playing = action(choice)


# It's longer, but now it's separated more logically based on what each piece is doing. The code
# directly in the while loop is only 3 lines and gives a better higher-level overview of what's going on.


# Function parameters respect the order in which the are given (there are special cases, but these are more advanced)
def orderedParameters(one, two, three, four):
    print("The first parameter: " + one)
    print("The second parameter: " + two)
    print("The third parameter: " + three)
    print("The fourth parameter: " + four)

one = "yes"
two = "no"
three = "hello"
four = "goodbye"

orderedParameters(three, two, four, one)

# You can also return more than one value, and again, order is important
def nameSwap(firstName, lastName):
    return lastName, firstName

firstName = "Dolan"
lastName = "Clayton"
firstName, lastName = nameSwap(firstName, lastName)
print("firstName: " + firstName)
print("lastName: " + lastName)

# Imagine an NPC that goes left to right across the screen until it hits the edge,
# and then goes back the other way
# Lets pretend it's a grid of 5 cells on 1 row, if the npc is moving to the right and gets to cell 4,
# change direction. If the npc is moving to the left and gets to cell 0, change direction
def move(position, direction):
    if direction > 0: #moving right
        if position == 4: #last space on the right, turn around
            direction *= -1
        else: #otherwise, move to the right
            position += 1
    else: #moving left
        if position == 0: #last space on the left, turn around
            direction *= -1
        else: #otherwise, move to the left
            position -= 1

    return position, direction


currentPosition = 0
currentDirection = 1
for i in range(20): #we'll do 20 move turns
    currentPosition, currentDirection = move(currentPosition, currentDirection)
    print("Position: " + str(currentPosition) + ", Direction: " + str(currentDirection))
