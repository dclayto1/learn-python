# Variables

# Variables are a way to store data and reference them later
a = 5
print(a) # 5

# You can store any datatype as a variable
b = "I'm a string!"
c = 5.2
d = True

# A variable represents the datatype of the value it is storing
# meaning that you can apply the same types of operations on the variable
# as you could on the datatype
print(a + c) # 10.2
print(b + str(a)) # I'm a string!5
print(a * a) # 25
print(a * b) # I'm a string!I'm a string!I'm a string!I'm a string!I'm a string!

# Variables are equal to the last value that was given to them
e = 5
e = 6
print(e) # 6

# Variables can change datatypes when being assigned
f = 1
print(f)
f = "I'm a string now"
print(f)
f = True
print(f)

# Variables have specific naming conventions
# they must start with a letter (lowercase or uppercase)
# they can contain numbers
# an underscore is the only special character allowed

aB3_Fg7 = "see?"
print(aB3_Fg7)

# camelCased
thisIsCamelCase = "one style of naming"

# underscore
this_is_underscore = "another style of naming"

# typically only Classes (advanced datatype) start with an uppercase letter
class Person:
    def __init__(self):
        return
person1 = Person()
print(person1)

# variable names should be descriptive so when you or someone else is reading
# your code, it is easier to see what is happening

# Good names
playerName = "Dolan"
health = 100
numberOfPotions = 4

# Bad names
pn = "Dolan"
hp = 100
np = 4

# You may ask, if I'm assigning the values right here, shouldn't I know what they are?
# Well, variables have what is called scope (a more advanced topic), and they live within their scope
# This example only has the 'global' scope. Now that we're far enough down, do you remember
# what the values of a and b are? Or even what datatype they are?
# Can we add a and b together?
aIs = a
myPoint = b
print("a is: " + str(a))
print("b is: " + str(myPoint))

# They are different datatypes and therefore we could not have simply added them together!
# But without good names, there isn't much context for what those variables were representing

# Variables can also store the result of operations of variables
num1 = 6
num2 = 7
result = num1 + num2
print(result) # 13


# Try creating a variable that represents a player's current health
# create another variable that represents damage that a bullet would do
# create another variable that represnts how much health a potion restores
# simulate what happens when the player is shot 2 times, picks up a potion, and then shot one more time
____
