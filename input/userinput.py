# Input: User Input
# A program without user input is pretty boring. The output is generally the same and there is no
# interactivity.

# Fortunately, we can have a user enter information!
print("Enter your name below:")
name = input()

print("\nHello, " + name + "!")

# If you want the user to enter information on the same line as the prompt, you can
# pass the prompt as a parameter to the function
month = input("What month were you born in?: ")
print("You were born in " + month)

# It's as simple as that. The input() function halts the program until the user presses the "Enter" or "Return"
# key.
print("We're going to add 2 numbers")
num1 = input("Enter the first number: ")
num2 = input("Enter the second number: ")
result = str((int(num1) + int(num2)))
print("The result of " + num1 + "+" + num2 + " is " + result)

print()
print()

# Now we're going to make a short madlib
boyName = input("Enter a boy's name: ")
monsterType = input("Enter a type of monster (singular): ")
skill = input("Enter a skill that you can learn: ")

print("There once was a boy named " + boyName + ". An evil " + monsterType + " took his")
print("princess captive. In order for " + boyName + " to save her, he needed to learn " + skill + ".")
print(boyName + " learned " + skill + " and saved his princess!")

# Try making your own short madlib
