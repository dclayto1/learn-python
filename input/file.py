# Input: File Input
# A common form a input is file input/output. You can read every line in a file.
# Depending on what you're doing, you make be reading a text file and counting the occurrences of
# specific words or letters. You could be reading in a csv (comma separated values) file like how
# data is formatted in spreadsheets, and perform calculations based on data in certain positions.
# You can also write out to a file. Say you are reading a spreadsheet that has 2 columns, num1 and num2,
# and you want to add a third column called avg. You can read in the csv, and for each line in the file,
# get the average of num1 and num2, and write out the 3rd column to the file

# We open a file like so
with open('example.txt', 'r') as inputFile:
    # Notice the indentation here. Python scopes everything by using indentation
    # values and variables that are created within this scoped section, live and die 
    # in this scoped section
    tmpVar = "Created within this scoped section"
    
    # 'example.txt' is the name (and path) of our file
    # 'r' means we're opening this file for read-only purposes
    # We know there are only 5 lines in the example file, and we want to read line by line
    line1 = inputFile.readline()
    print(line1)
    line2 = inputFile.readline()
    print(line2)
    line3 = inputFile.readline()
    print(line3)
    line4 = inputFile.readline()
    print(line4)
    line5 = inputFile.readline()
    print(line5)

    # The readline() function reads in the entire line as a string datatype
    # We know that each of these lines is a float value
    # With that, print out the average value of the example file
    print(____)



# tmpVar is no longer available to use for use since we're not in that scoped section
# print(tmpVar) # this will throw an error

# There are actually 2 ways to open files. The method above is the safe way of opening files
# as it automatically closes the file when we leave that scope section. If it didn't you as the
# programmer would have to remember, and then also write extra error handling code in case the
# program crashed after opening the file, but before you got to the part where you close it!

