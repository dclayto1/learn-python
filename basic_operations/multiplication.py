# Basic Operations: Multiplication

# Multiplying integers
print(1 * 1) # 1
print(-5 * -3) # 15
print(0 * 1 * 2 * 3) # 0
print(-2 * 1) # -2

# Multiplying floats (floats are weird and I never knew this)
print(0.0 * 1.2) # 0.0
print(-4.2 * -1.8) # 7.5600000000000005
print(1.1 * 2.2) # 2.4200000000000004

# Multiplying integers and floats (the output becomes a float)
print(0 * 1.2) # 0
print(5 * -4.1) # -20.5
print(-4 * -9.0) # 36.0

# Strings can be multiplied by integers
print(4 * "hello") # hellohellohellohello
print("why " * 2) # why why 

# When multiplying boolean values, they take their 
# binary representation 1 and 0, for True and False respectively
print(True * True) # 1
print(False * False) # 0
print(True * False) # 0

# Try multiplying integers
print(____)

# Try multiplying floats
print(____)

# Try multiplying floats and integers
print(____)

# Try multiplying integers and strings
print(____)

# Using string multiplication and addition, create the string: "Hello Hello Monkey Man Monkey Man"
print(____)
