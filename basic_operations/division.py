# Basic Operations: Division

# Division integers
# In python3, integer division results in float values (otherwise known as doing float division)
print(2 / 1) # 2.0
print(7 / 2) # 3.5

# In python2 and most other programming languages, integer division
# would result in the floor value of the quotient. We can achieve this in python3
# by converting the resulting value to an integer
print(int(2 / 1)) # 2
print(int(7 / 2)) # 3

# Programming also has a neat feature called modulus division. This is also called
# remainder division
# Since you can do integer division which would take the floor value, you'd be missing out on
# the remainder value. The above example: 7/2 equals 3, but how do you get the remainder?
# You could math it up and do 7/2 equals 3. 3 * 2 equals 6. 7 - 6 equals 1. So the remainder
# is 1. Or you can use modulus division!
print(7 % 2) # 1
print(7 % 4) # 3
print(100 % 21) # 16
print(13 % 13) # 0

# Dividing floats (floats are weird and I never knew this)
print(0.0 / 1.2) # 0.0
print(-4.2 / -1.8) # 2.3333333333333335
print(1.1 / 2.2) # 0.5

# You cannot divide by 0. You'll get a "ZeroDivisionError"
# Uncomment below if you'd like to see
# print(1/0)

# Strings can not be divided

# Since you cannot divide by zero, dividing booleans doesn't make much since since you can
# only divide by True and not False (remember that they convert to their binary representations of 1 and 0)
print(True / True) # 1
print(False / True) # 0

# Try dividing integers
print(____)

# Try modulus division
print(____)

# Try dividing floats
print(____)
