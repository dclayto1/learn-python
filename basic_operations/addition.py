# Basic Operations: Addition
# The basic datatypes can be added together

# Adding integers
print(1 + 1) # 2
print(-5 + -3) # -8
print(0 + 1 + 2 + 3) # 6
print(-2 + 1) # -1

# Adding floats
print(0.0 + 1.2) # 1.2
print(-4.2 + -1.8) # -6.0
print(-1.0 + 0.5) # -0.5

# Adding integers and floats (the output becomes a float)
print(0 + 1.2) # 1.2
print(5 + -2.5) # 2.5
print(4.0 + 9.0) # 13.0

# Adding strings together combines them!
print("Hello" + "World") # HelloWorld
print("Hello" + " " + "World") # Hello World
print("Hello " + "World") # Hello World

# You can also add strings with integers or floats using the str() function
# The str() function tries to convert the value to a String datatype
print("Number " + str(1)) # Number 1
print(str(2) + " is better than " + str(1)) # 2 is better than 1
print(str(2) + " + 2 = " + str(2+2)) # 2 + 2 = 4

# Boolean values are a little different. When adding boolean values, they take their 
# binary representation 1 and 0, for True and False respectively
print(True + True) # 2
print(False + False) # 0
print(True + False) # 1
print(True + True + True + False) # 3

# Try adding integers
print(____)

# Try adding floats
print(____)

# Try adding floats and integers
print(____)

# Try adding an integer or float with a string
print(____)
