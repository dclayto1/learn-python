# Basic Operations: Subtraction
# Some basic datatypes can be subtracted from each other

# Subtracting integers
print(1 - 1) # 0
print(-5 - -3) # -2
print(0 - 1 - 2 - 3) # -6
print(-2 - 1) # -3

# Subtracting floats doesn't actually work as expected (just found this out, who knew?)
print(0.0 - 1.2) # -1.2
print(-4.2 - -1.8) # -2.4000000000000004 
print(-1.0 - 0.5) # -1.5
print(1.1 - 0.5) # 0.6000000000000001

# Adding integers and floats (the output becomes a float)
print(0 - 1.2) # -1.2
print(5 - -2.5) # 7.5

# Strings cannot be subtracted!

# When subtracting boolean values, they take their binary representation 1 and 0
print(True - True) # 0
print(False - False) # 0
print(True - False) # 1
print(True - True - True - False) # -1

# Try subtracting integers
print(____)

# Try subtracting floats
print(____)

# Try subtracting floats and integers
print(____)
