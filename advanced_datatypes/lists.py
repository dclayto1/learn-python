# Lists
# Lists are an ordered container of data. A list in Python can contain any amount of data, and any type of data.
firstList = [1, 2, 3, 4]

# These lists can also contain mixed data types.
def get5():
    return 5
mixedList = ['im a string', True, False, 123, 98.34, get5()]

# You can access an individual item from a list with its positional index
lst = [1, 2, 5, 0, 3]
print(lst[0]) # 1
print(lst[3]) # 0
# print(lst[100]) #Error because this list doesn't have that many indices

# You can wrap backwards through a list too. If 0 is the first item, then -1 is the last item
print(lst[-1]) # 3 
print(lst[-2]) # 0
# print(lst[-100]) # Error because the list doesn't have that many indices backward

# Common List methods: append, insert, remove, index, sort, reverse
lst.append(9) # Adds 9 to the end of the list. lst == [1,2,5,0,3,9]
lst.insert(0, 2) # Inserts 2 into index 0 of the list, and shifts everything after that index to the right. lst == [2,1,2,5,0,3,9]
lst.remove(5) # Removes the first occurrence of the parameter (or throws an error if not found) and shifts everything after that occurrence to the left. lst = [2,1,2,0,3,9]
print(lst.index(0)) # gets the index of the first occurrence of the parameter. This prints 3
lst.sort() # this sorts the list. the variable lst is changed to a sorted list. lst == [0,1,2,2,3,9]
lst.reverse() # this reverses the list. the variable lst is changed to the reversed list. lst == [9,3,2,2,1,0]


# Common methods used on lists: len, max, min, sum
print(len(lst)) # gets the length of the list. this prints 6
print(max(lst)) # gets the max element of the list. this prints 9
print(min(lst)) # gets the min element of the list. this prints 0
print(sum(lst)) # sums the elements of the list. this only makes sense if your items are numerical. this prints 17

# You can also print a list
print(lst)

# You can iterate over a list
index = 0
for item in lst:
    print(str(index) + ": " + str(item))
    index += 1
# 0: 9
# 1: 3
# 2: 2
# 3: 2
# 4: 1
# 5: 0

# Strings can be thought of as a list of characters
helloString = "Hello"
helloList = ['H', 'e', 'l', 'l', 'o']
# helloString and helloList are not equal to each other, but you can think of the string in terms of the list
print(helloString[0] == helloList[0])
print(helloString[1] == helloList[1])
print(helloString[2] == helloList[2])
print(helloString[3] == helloList[3])
print(helloString[4] == helloList[4])



# A single list is one dimensional. However, lists can contain lists which give the ability to use multiple dimensions
# A common example of a 2-dimensional list is a grid
gameGrid = [[0,0,0,0,0],[0,0,0,0,0],[0,0,1,0,0]]
gameGridReadable = [
        [0,0,0,0,0],
        [0,0,0,0,0],
        [0,0,1,0,0]
    ]

# Lists are also commonly used to represent Vectors(1D list) or Matrices (multi-D list)
vectorA = [3, 4]
vectorB = [1, 5]

# Another example usage of lists are to represent stacks and queues which are commonly used data structures in computer science

# A "stack" is a LIFO list. Last In, First Out
# A practical example is a stack of plates. When you put away plates, you stack them on top of each other.
# The first plate put away is at the bottom of the stack. The last plate put away is at the top of the stack.
# When you go to grab a plate to use the next time, you take it off of the top of the plate stack. Therefore,
# The last plate is the first to be grabbed. LIFO
exampleStack = ['p1', 'p2', 'p3', 'p4']
# Visualized like:
# 'p4' - TOP
# 'p3'
# 'p2'
# 'p1' - BOTTOM
# If we were to make an actual implementation of a stack, we would create functions like:
# add(item) #Add an item to the end of the list
# pop(item) #Pop an item off the end of the list and return it


# A "queue" is a FIFO list. First In, First Out
# A practical example is a drivethru. Many cars may come to a drivethru, but then it gets backed up and creates
# a line, or a queue. The first person to get to the drivethru is the first person to get their food and
# leave the drivethru.
exampleQueue = ['c1', 'c2', 'c3', 'c4']
# To visualize this, think of the left as being the front of the line, and the right being the end of the line
# New cars pull up to the end of the line, and the flow of the drivethru moves from the right to the left.
# When the car at the front is done, all the cars after it in the list, move 1 space to the left
# OUT <-- 'c1', 'c2', 'c3', 'c4' <-- IN
# If we were to make an actual implementation of a queue, we would create functions like:
# queue(item) #Add an item to the end of the list
# dequeue() #Remove item from the front of the list and shift items to the left (python's remove() function for lists does this already)


# Vector addition: http://mathworld.wolfram.com/VectorAddition.html
# A + B = (a1+b1, a2+b2, ..., an+bn)
#  _   _     __
# |4| |9| = |13|
# |3| |1|   | 4|
#  -   -     --
#
# Write a function that accepts 2 vectors of any size as parameters, adds them together, and returns the new vector. 
# Vector addition requires the vectors to be of the same dimensions, so you need to verify that they are,
# and if they aren't return None
def addVectors():
    pass


# Using the logic/code from the functions lesson where we created a simple NPC that moves left and right
# Create a way to display it using a grid length of 10
playing = True
while playing:
    # update NPC position

    # display grid
    continue
